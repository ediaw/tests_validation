﻿namespace TennisTests;

public class TennisTests
{
    [Theory]
    [InlineData(1, 0, "15-0")]
    [InlineData(2, 3, "30-40")]
    [InlineData(4, 0, "Player 1 wins !")]
    [InlineData(4, 2, "Player 1 wins !")]
    [InlineData(5, 4, "Avantage-40")]
    [InlineData(5, 5, "40-40")]
    [InlineData(4, 6, "Player 2 wins !")]
    public void TestScore(int p1Score, int p2Score, string score)
    {
        Assert.Equal(score, Tennis.TennisGame.GetScore(p1Score, p2Score));
    }

}

﻿namespace Tennis
{
	public class TennisGame
	{
        static void Main()
        {
            int player1Score = 0;
            int player2Score = 0;

            PlayerScores(player1Score);
            PlayerScores(player2Score);
            PlayerScores(player1Score);
            PlayerScores(player1Score);
            PlayerScores(player2Score);
            PlayerScores(player2Score);
            PlayerScores(player1Score);

            GetScore(player1Score, player2Score);
        }

        public static string GetScore(int p1Score, int p2Score)
        {
            if (p1Score >= 4 && p1Score >= p2Score + 2)
            {
                return "Player 1 wins !";
            }
            else if (p2Score >= 4 && p2Score >= p1Score + 2)
            {
                return "Player 2 wins !";
            }
            else if (p1Score > 3 && p1Score == p2Score + 1)
            {
                return "Avantage-40";
            }
            else if (p2Score > 3 && p2Score == p1Score + 1)
            {
                return "40-Avantage";
            }
            else if (p1Score > 3 && p1Score == p2Score)
            {
                return "40-40";
            }
            else
            {
                return WriteScore(p1Score) + "-" + WriteScore(p2Score);
            }
        }

        public static string WriteScore(int playerScore)
        {
            switch (playerScore)
            {
                case 0:
                    return "0";
                case 1:
                    return "15";
                case 2:
                    return "30";
                case 3:
                    return "40";
                default:
                    return "";
            }
        }

        public static void PlayerScores(int playerScore)
        {
            playerScore++;
        }


    }
}


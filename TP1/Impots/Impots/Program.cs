﻿using System;
namespace Impots
{
    public static class Impots
    {

        public static double CalculerTauxImposition(int userSalary)
        {

            if (userSalary <= 10777)
            {
                return 0.0;
            }
            else if ( userSalary <= 27478)
            {
                return 0.11;
            }
            else if (userSalary <= 78570)
            {
                return 0.30;
            }
            else if (userSalary <= 168994)
            {
                return 0.41;
            }
            else
            {
                return 0.45;
            }
        }


        public static double CalculerMontant(int userSalary)
        {
            return userSalary * CalculerTauxImposition(userSalary);
        }

        public static bool IsInRange(int userSalary)
        {
            return userSalary >= 0;
        }
    }
}



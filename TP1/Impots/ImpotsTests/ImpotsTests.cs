﻿namespace ImpotsTests;

public class ImpotsTests
{
    [Fact]
    public void SalaryInRange()
    {
        Assert.True(Impots.Impots.IsInRange(0));
        Assert.True(Impots.Impots.IsInRange(10000));
        Assert.False(Impots.Impots.IsInRange(-2));
    }


    [Theory]
    [InlineData(10777, 0.0)]
    [InlineData(10778, 0.11)]
    [InlineData(27479, 0.30)]
    [InlineData(78571, 0.41)]
    [InlineData(168995, 0.45)]
    public void TestCalculerTauxImposition(int revenu, double tauxImposition)
    {
        Assert.Equal(tauxImposition, Impots.Impots.CalculerTauxImposition(revenu));
    }

    [Theory]
    [InlineData(10777, 10777)]
    [InlineData(10778, 1185.58)]
    [InlineData(27479, 8243.7)]
    [InlineData(78571, 32214.11)]
    [InlineData(168995, 76047.75)]
    public void TestCalculerMontant(int revenu, double montant)
    {
        Assert.Equal(montant*, Impots.Impots.CalculerMontant(revenu));
    }
}

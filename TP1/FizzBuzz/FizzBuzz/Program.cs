﻿using System;
namespace FizzBuzz
{
    public static class FizzBuzz
    {
        public static void Main()
        {
            int userInput = GetUserNumber();
            Console.WriteLine(Generer(userInput));

        }

        public static string Generer(int n)
        {
            string list = "";

            for(int i=1; i<=n;i++)
            {
                if (i % 3 == 0 && i % 5 == 0)
                {
                    list += "FizzBuzz";
                }
                else if (i % 5 == 0)
                {
                    list += "Buzz";
                }
                else if (i % 3 == 0)
                {
                    list += "Fizz";
                }
                else
                {
                    list += i.ToString();
                }
            }

            return list;
        }

        public static int GetUserNumber()
        {
            Console.WriteLine("Please write a number between 15 and 150");
            int userNb = int.Parse(Console.ReadLine());

            while(!IsInRange(userNb))
            {
                Console.WriteLine("Please write a number between 15 and 150");
                userNb = int.Parse(Console.ReadLine());
            }
            return userNb;
        }

        public static bool IsInRange(int userNb)
        {
            return userNb >= 15 && userNb <= 150;
        }
    }
}


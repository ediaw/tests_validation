﻿namespace FizzBuzzTests;

public class UnitTest1
{
    [Fact]
    public void isFizz()
    {
        int n = 3;
        string result = FizzBuzz.FizzBuzz.Generer(n);
        Assert.Equal("12Fizz", result);

    }

    [Fact]
    public void isBuzz()
    {
        int n = 10;
        string result = FizzBuzz.FizzBuzz.Generer(n);
        Assert.Equal("12Fizz4BuzzFizz78FizzBuzz", result);
    }

    [Fact]
    public void isFizzBuzz()
    {
        int n = 15;
        string result = FizzBuzz.FizzBuzz.Generer(n);
        Assert.Equal("12Fizz4BuzzFizz78FizzBuzz11Fizz1314FizzBuzz", result);
    }

    [Fact]
    public void inRange()
    {
        Assert.True(FizzBuzz.FizzBuzz.IsInRange(15));
        Assert.True(FizzBuzz.FizzBuzz.IsInRange(150));
        Assert.False(FizzBuzz.FizzBuzz.IsInRange(0));
        Assert.False(FizzBuzz.FizzBuzz.IsInRange(160));
    }

}

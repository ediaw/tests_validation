﻿using System;
namespace CreditMensuel
{
	public class CreditImmobilier
	{
        public static bool isCapitalHigherThan50k(int capital)
        {
            return capital >= 50000;
        }

        public static bool isDurationInRange(int yearsOfDuration)
        {
            return yearsOfDuration >= 9 && yearsOfDuration <= 25;
        }

        public static bool isRateInRange(int chosenRate)
        {
            return chosenRate >= 1 && chosenRate <= 3;
        }

        public static double CalculateMonthlyPaymentWithoutAssurance(int capital, int duration, double rate)
        {
            int durationInMonth = 12 * duration;
            double tmpRate = (rate/100) / 12;
            return Math.Round((capital * tmpRate) / (1 - Math.Pow(1 + tmpRate, -durationInMonth)), 2);
        }

        public static double CalculateAssuranceRate(bool sporty, bool smoker, bool cardiac, bool engineer, bool pilot)
        {
            double assuranceRate = 0.3;
            if(sporty)
            {
                assuranceRate -= 0.05;
            }
            if (smoker)
            {
                assuranceRate += 0.15;
            }
            if (cardiac)
            {
                assuranceRate += 0.3;
            }
            if (engineer)
            {
                assuranceRate -= 0.05;
            }
            if (pilot)
            {
                assuranceRate += 0.15;
            }
            return Math.Round(assuranceRate,2);
        }

        public static double CalculateMonthlyPaymentWithAssurance(double monthlyPayment, double assurancePayment)
        {
            return Math.Round(monthlyPayment+assurancePayment, 2);
        }

        public static double GetTaux(int niveauTaux, int duree)
        {
            double taux = 0;
            if(niveauTaux == 1)
            {
                if(duree == 9)
                {
                    taux = 2.56;
                }
                else if (duree == 10)
                {
                    taux = 2.63;
                }
                else if (duree == 15)
                {
                    taux = 2.79;
                }
                else if (duree == 20)
                {
                    taux = 2.90;
                }
                else if (duree == 25)
                {
                    taux = 3.00;
                }
            }
            else if (niveauTaux == 2)
            {
                if (duree == 9)
                {
                    taux = 2.37;
                }
                else if (duree == 10)
                {
                    taux = 2.47;
                }
                else if (duree == 15)
                {
                    taux = 2.67;
                }
                else if (duree == 20)
                {
                    taux = 2.76;
                }
                else if (duree == 25)
                {
                    taux = 2.88;
                }
            }
            else
            {
                if (duree == 9)
                {
                    taux = 2.10;
                }
                else if (duree == 10)
                {
                    taux = 2.15;
                }
                else if (duree == 15)
                {
                    taux = 2.33;
                }
                else if (duree == 20)
                {
                    taux = 2.33;
                }
                else if (duree == 25)
                {
                    taux = 2.45;
                }
            }
            return taux;
        }

        public static double CalculateAssurancePaymentPerMonth(double assuranceRate, int capital, int duration)
        {
            int durationInMonths = duration * 12;
            return Math.Round(assuranceRate / 100 * capital /durationInMonths, 2);
        }

        public static double CalculateAssurancePayment(double assuranceRate, int capital, int duration)
        {
            int durationInMonths = duration * 12;
            return Math.Round(assuranceRate / 100 * capital, 2);
        }
    }  
}


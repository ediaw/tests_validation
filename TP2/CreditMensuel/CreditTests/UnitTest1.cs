using static System.Runtime.InteropServices.JavaScript.JSType;

namespace CreditTests;

public class CreditImmobilierTests
{
    [Fact]
    public void CapitalIsHigherThan50000()
    {
        Assert.True(CreditMensuel.CreditImmobilier.isCapitalHigherThan50k(50000));
        Assert.True(CreditMensuel.CreditImmobilier.isCapitalHigherThan50k(51000));
        Assert.False(CreditMensuel.CreditImmobilier.isCapitalHigherThan50k(0));
    }

    [Fact]
    public void DurationIsHigherThan9AndLowerThan25()
    {
        Assert.True(CreditMensuel.CreditImmobilier.isDurationInRange(9));
        Assert.True(CreditMensuel.CreditImmobilier.isDurationInRange(20));
        Assert.False(CreditMensuel.CreditImmobilier.isDurationInRange(26));
    }

    [Fact]
    public void RateIsBetween1And3()
    {
        Assert.True(CreditMensuel.CreditImmobilier.isRateInRange(2));
        Assert.False(CreditMensuel.CreditImmobilier.isRateInRange(0));
        Assert.False(CreditMensuel.CreditImmobilier.isRateInRange(5));
    }

    [Theory]
    [InlineData(50000, 15, 2.79, 340.26)]
    [InlineData(50000, 20, 2.76, 271.33)]
    [InlineData(75000, 25, 2.45, 334.58)]
    public void isMonthlyPaymentWithoutAssuranceOK(int capital, int duration, double rate, double expectedMonthlyPayment)
    {
        Assert.Equal(expectedMonthlyPayment, CreditMensuel.CreditImmobilier.CalculateMonthlyPaymentWithoutAssurance(capital, duration, rate));
    }

    [Theory]
    [InlineData(false, false, false, false, false, 0.30)]
    [InlineData(true, false, false, false, false, 0.25)]
    [InlineData(false, true, false, false, false, 0.45)]
    [InlineData(false, false, true, true, false, 0.55)]
    public void isAssuranceRateOK(bool sporty, bool smoker, bool cardiac, bool engineer, bool pilot, double expectedAssuranceRate)
    {
        Assert.Equal(expectedAssuranceRate, CreditMensuel.CreditImmobilier.CalculateAssuranceRate(sporty, smoker, cardiac, engineer, pilot));
    }

    [Theory]
    [InlineData(1,10,2.63)]
    [InlineData(2,15,2.67)]
    [InlineData(3,25,2.45)]
    public void isRateTableOK(int niveauTaux, int duree, double expectedTaux)
    {
        Assert.Equal(expectedTaux, CreditMensuel.CreditImmobilier.GetTaux(niveauTaux,duree));
    }
}